<?php
/**
 * Created by PhpStorm.
 * User: Corentin
 * Date: 06/06/2018
 * Time: 17:13
 */


require_once('config.php');


function checkMailExists(User $user)
{
    $dbh = bddConnect();
    $mail = $user->getEmail();
    $stmt = $dbh->prepare("SELECT * FROM users WHERE email = :email ");
    $stmt->bindParam(':email', $mail);
    $stmt->execute();
    $result = $stmt->fetchAll();
    if (!empty($result)) {
        return true;
    } else {
        return false;
    }
}

function checkIfMailExistsAnotherUser(User $user)
{
    $dbh = bddConnect();
    $email = $user->getEmail();
    $userId = $user->getId();
    $stmt = $dbh->prepare('SELECT * FROM users WHERE email=:email AND id <> :id');
    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':id', $userId);
    $stmt->execute();
    $data = $stmt->fetchAll();
    if (!empty($data)) {
        return true;
    } else {
        return false;
    }
}

function addUser(User $user)
{
    $dbh = bddConnect();
    //add
    $role = 2;
    $nom = $user->getNom();
    $prenom = $user->getPrenom();
    $user->getEmail();
    $hashedPass = hash('sha1', $user->getMdp());
    $stmt = $dbh->prepare("INSERT INTO Users (email, nom, password, prenom, role) VALUES (:email, :nom, :password, :prenom, :role)");
    $stmt->bindParam(':password', $hashedPass);
    $stmt->bindParam(':nom', $nom);
    $stmt->bindParam(':prenom', $prenom);
    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':role', $role);
    $stmt->execute();
}

function loginUser(User $user)
{
    $bdd = bddConnect();
    $mail = $user->getEmail();
    $hashedpass = hash('sha1', $user->getMdp());
    $stmt = $bdd->prepare("SELECT * FROM users WHERE email = :email AND password = :password ");
    $stmt->bindParam(':password', $hashedpass);
    $stmt->bindParam(':email', $mail);
    $stmt->execute();
    $result = $stmt->fetch();
    if (!empty($result)) {
        return new User($result['prenom'], $result['nom'], $result['email'], $result['role'], null, $result['id']);
    } else {
        return false;
    }
}

function updateUser(User $user)
{
    $dbh = bddConnect();
    $userId = $user->getId();
    $nom = $user->getNom();
    $prenom = $user->getPrenom();
    $email = $user->getEmail();
    $role = $user->getRole();
    $hashNewPwd = hash('sha1', $user->getMdp());

    $stmt = $dbh->prepare('UPDATE users SET nom=:nom, prenom=:prenom, email=:email, password=:password, role=:role WHERE id=:id');
    $stmt->bindParam(':id', $userId);
    $stmt->bindParam(':nom', $nom);
    $stmt->bindParam(':prenom', $prenom);
    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':role', $role);
    $stmt->bindParam(':password', $hashNewPwd);
    $stmt->execute();
}

function deleteUser(User $user)
{
    $dbh = bddConnect();
    $userId = $user->getId();
    $stmt = $dbh->prepare('DELETE FROM users WHERE id=:id');
    $stmt->bindParam(':id', $userId);
    $stmt->execute();
}

function getAllUsers()
{
    $dbh = bddConnect();
    $stmt = $dbh->prepare('SELECT * FROM users');
    $stmt->execute();
    $data = $stmt->fetchAll();

    foreach ($data as $rows) {
        $usersResult[] = new User($rows['prenom'], $rows['nom'], $rows['email'], $rows['role'], null, $rows['id']);
    }

    return $usersResult;
}

function getUserById(int $id)
{

    $dbh = bddConnect();
    $stmt = $dbh->prepare('SELECT * FROM users WHERE id=:id');
    $stmt->bindParam(':id', $id);

    $stmt->execute();

    $data = $stmt->fetch();

    return new User($data['prenom'], $data['nom'], $data['email'], $data['role'], $data['password'], $id);

}
function getUserByEmail(string $email)
{

    $dbh = bddConnect();
    $stmt = $dbh->prepare('SELECT * FROM users WHERE email=:email');
    $stmt->bindParam(':email', $email);

    $stmt->execute();

    $data = $stmt->fetch();

    return new User($data['prenom'], $data['nom'], $email, $data['role'], $data['password'], $data['id']);

}

function getRole()
{
    $dbh = bddConnect();
    $stmt = $dbh->prepare('SELECT * FROM role');
    $stmt->execute();

    $data = $stmt->fetchAll();

    foreach ($data as $rows) {
        $roles[] = new Role($rows['nom_role'], $rows['id']);
    }

    return $roles;
}
function generatePwd($size)
{
    // Initialisation des caractères utilisables
    $characters = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
    $password= null;
    for($i=0;$i<$size;$i++)
    {
        $password .= ($i%2) ? strtoupper($characters[array_rand($characters)]) : $characters[array_rand($characters)];
    }

    return $password;
}