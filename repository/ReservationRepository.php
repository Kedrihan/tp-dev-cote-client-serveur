<?php
/**
 * Created by PhpStorm.
 * User: Corentin
 * Date: 07/06/2018
 * Time: 11:26
 */

require_once('config.php');
require_once('BilletRepository.php');

function addReservation(Reservation $resa)
{
    $dbh = bddConnect();
    $billetId = $resa->getBilletId();
    $clientId = $resa->getClientId();
    $quantite = $resa->getQuantite();

    $stmt = $dbh->prepare("INSERT INTO reservation (billet, client, quantite) VALUES (:billet, :client, :quantite)");
    $stmt->bindParam(':billet', $billetId);
    $stmt->bindParam(':client', $clientId);
    $stmt->bindParam(':quantite', $quantite);

    $stmt->execute();
    $billet = getBilletById($billetId);
    $newQuantite = $billet->getQuantite() - $quantite;
    $billet->setQuantite($newQuantite);
    if ($newQuantite == 0) {
        $billet->setEstActif(0);
    }
    updateBillet($billet);
}

function annulationReservation(int $id, int $idBillet)
{

    $dbh = bddConnect();
    $billet = getBilletById($idBillet);
    $resa = getReservationById($id);
    $newQuantite = $billet->getQuantite() + $resa->getQuantite();
    if ($newQuantite > 0) {
        $billet->setEstActif(1);
    }
    $billet->setQuantite($newQuantite);
    updateBillet($billet);
    $resaId = $resa->getId();
    $stmt = $dbh->prepare("DELETE FROM reservation WHERE id=:id");
    $stmt->bindParam(':id', $resaId);

    $stmt->execute();


}

function getReservationById(int $id)
{
    $dbh = bddConnect();
    $stmt = $dbh->prepare('SELECT * FROM reservation WHERE id=:id');
    $stmt->bindParam(':id', $id);

    $stmt->execute();

    $data = $stmt->fetch();

    return new Reservation($data['billet'], $data['client'], $data['quantite'], $id);;
}

function getReservationByClientId(int $clientid)
{
    $dbh = bddConnect();
    $stmt = $dbh->prepare('SELECT * FROM reservation WHERE client=:id');
    $stmt->bindParam(':id', $clientid);

    $stmt->execute();

    $data = $stmt->fetchAll();
    if (!empty($data)) {
        foreach ($data as $rows) {

            $resaResult[] = new Reservation($rows['billet'], $rows['client'], $rows['quantite'], $rows['id']);
        }
        return $resaResult;
    }
    return null;


}

function getAllReservationForBillet(Billet $billet)
{
    $dbh = bddConnect();
    $billetId = $billet->getId();
    $stmt = $dbh->prepare('SELECT * FROM reservation where billet=:id');
    $stmt->bindParam(':id', $billetId);
    $stmt->execute();
    $data = $stmt->fetch();

    foreach ($data as $rows) {
        $resaResult[] = new Reservation($rows['billet'], $rows['client'], $rows['quantite'], $rows['id']);
    }

    return $resaResult;
}

function getBilletReservationForClient(Billet $billet, User $user)
{
    $dbh = bddConnect();
    $bill = $billet->getId();
    $us = $user->getId();
    $stmt = $dbh->prepare('SELECT r.id FROM reservation AS r INNER JOIN users as u ON r.client = u.id INNER JOIN billets_dispo as b ON r.billet=b.id WHERE billet=:id AND client=:client');
    $stmt->bindParam(':id', $bill);
    $stmt->bindParam(':client', $us);
    $stmt->execute();
    $data = $stmt->fetch();

    foreach ($data as $rows) {
        $resaResult[] = $rows;
    }

    return $resaResult;
}

function verifyDuplicata(Reservation $resa) {
    $dbh = bddConnect();
    $billet = $resa->getBilletId();
    $client = $resa->getClientId();
    $stmt = $dbh->prepare("SELECT * FROM reservation WHERE billet = :billet AND client=:client ");
    $stmt->bindParam(':billet', $billet);
    $stmt->bindParam(':client', $client);
    $stmt->execute();
    $result = $stmt->fetchAll();
    if (!empty($result)) {
        return true;
    } else {
        return false;
    }
}