<?php
/**
 * Created by PhpStorm.
 * User: Corentin
 * Date: 06/06/2018
 * Time: 17:13
 */


require_once('config.php');


function addBillet(Billet $billet)
{
    $dbh = bddConnect();
    //add
    $actif = 1;
    $depart = $billet->getGareDepart();
    $arrivee = $billet->getGareArrivee();
    $datehdep = $billet->getDateHeureDepart()->format('Y-m-d H:i');
    $duree = $billet->getDureeTrajet()->format('H:i');
    $qtt = $billet->getQuantite();
    $numerot = $billet->getNumeroTrain();
    $stmt = $dbh->prepare("INSERT INTO billets_dispo (gare_depart, gare_arrivee, dateheure_depart, duree_trajet, quantite_dispo, numero_train, estActif) VALUES (:gare_depart, :gare_arrivee, :dateheure_depart, :duree_trajet, :quantite_dispo, :numero_train, :estActif)");
    $stmt->bindParam(':gare_depart', $depart);
    $stmt->bindParam(':gare_arrivee', $arrivee);
    $stmt->bindParam(':dateheure_depart', $datehdep);
    $stmt->bindParam(':duree_trajet', $duree);
    $stmt->bindParam(':quantite_dispo', $qtt);
    $stmt->bindParam(':numero_train', $numerot);
    $stmt->bindParam(':estActif', $actif);
    $stmt->execute();
}


function updateBillet(Billet $billet)
{
    $dbh = bddConnect();
    $depart = $billet->getGareDepart();
    $arrivee = $billet->getGareArrivee();
    $datehdep = $billet->getDateHeureDepart()->format('Y-m-d H:i:s');
    $duree = $billet->getDureeTrajet()->format('H:i:s');
    $qtt = $billet->getQuantite();
    $numerot = $billet->getNumeroTrain();
    $actif = $billet->getEstActif();
    $idB = $billet->getId();
    $stmt = $dbh->prepare('UPDATE billets_dispo SET gare_depart=:gare_depart, gare_arrivee=:gare_arrivee, dateheure_depart=:dateheure_depart, duree_trajet=:duree_trajet, quantite_dispo=:quantite_dispo, numero_train=:numero_train, estActif=:estActif WHERE id=:id');
    $stmt->bindParam(':gare_depart', $depart);
    $stmt->bindParam(':id', $idB);
    $stmt->bindParam(':gare_arrivee', $arrivee);
    $stmt->bindParam(':dateheure_depart', $datehdep);
    $stmt->bindParam(':duree_trajet', $duree);
    $stmt->bindParam(':quantite_dispo', $qtt);
    $stmt->bindParam(':numero_train', $numerot);
    $stmt->bindParam(':estActif', $actif);
    $stmt->execute();
}

function deleteBillet(Billet $billet)
{
    $dbh = bddConnect();
    $idB = $billet->getId();
    $stmt = $dbh->prepare('DELETE FROM billets_dispo WHERE id=:id');
    $stmt->bindParam(':id', $idB);
    $stmt->execute();
}

function getAllBillets()
{
    $dbh = bddConnect();
    $stmt = $dbh->prepare('SELECT * FROM billets_dispo WHERE estActif = 1');
    $stmt->execute();
    $data = $stmt->fetch();

    foreach ($data as $rows) {
        $dateheuredepart = new DateTime($rows['dateheure_depart']);
        $duree = new DateTime($rows['duree_trajet']);
        $numTrain = (int)$rows['numero_train'];
        $billetResult[] = new Billet($rows['gare_depart'], $rows['gare_arrivee'], $duree, $dateheuredepart, $numTrain, $rows['quantite_dispo'], $rows['estActif'], $rows['id']);
    }

    return $billetResult;

}

function getBilletById(int $id)
{

    $dbh = bddConnect();
    $stmt = $dbh->prepare('SELECT * FROM billets_dispo WHERE id=:id');
    $stmt->bindParam(':id', $id);

    $stmt->execute();

    $data = $stmt->fetch();
    $dateheuredepart = new DateTime($data['dateheure_depart']);
    $duree = new DateTime($data['duree_trajet']);
    $numTrain = (int)$data['numero_train'];
    return new Billet($data['gare_depart'], $data['gare_arrivee'], $duree, $dateheuredepart, $numTrain, $data['quantite_dispo'], $data['estActif'], $id);

}

function searchBillet(string $gareDepartQuery, string $gareArriveeQuery, DateTime $dateHeureDepartQuery)
{
    $dbh = bddConnect();
    $gareDepartQuery = '%' . $gareDepartQuery . '%';
    $gareArriveeQuery = '%' . $gareArriveeQuery . '%';
    $dateHeureDepartQuery = $dateHeureDepartQuery->format('Y-m-d H:i:s');
    $stmt = $dbh->prepare('SELECT * FROM billets_dispo WHERE (gare_depart LIKE :gareDepart AND gare_arrivee LIKE :gareArrivee AND dateheure_depart>=STR_TO_DATE(:dateheure_depart, "%Y-%m-%d %H:%i:%s") AND estActif=1)');
    $stmt->bindParam(':dateheure_depart', $dateHeureDepartQuery);
    $stmt->bindParam(':gareDepart', $gareDepartQuery);
    $stmt->bindParam(':gareArrivee', $gareArriveeQuery);
    $stmt->execute();

    $data = $stmt->fetchAll();

    foreach ($data as $rows) {
        $dateheuredepart = new DateTime($rows['dateheure_depart']);
        $duree = new DateTime($rows['duree_trajet']);
        $numTrain = (int)$rows['numero_train'];
        $billetResult[] = new Billet($rows['gare_depart'], $rows['gare_arrivee'], $duree, $dateheuredepart, $numTrain, $rows['quantite_dispo'], $rows['estActif'], $rows['id']);
    }
    return $billetResult;
}

function getGaresDepart()
{
    $dbh = bddConnect();
    $stmt = $dbh->prepare('SELECT gare_depart FROM billets_dispo GROUP BY gare_depart');
    $stmt->execute();
    $data = $stmt->fetchAll();
    return $data;
}

function getGaresArrivee()
{
    $dbh = bddConnect();
    $stmt = $dbh->prepare('SELECT gare_arrivee FROM billets_dispo GROUP BY gare_arrivee');
    $stmt->execute();
    $data = $stmt->fetchAll();
    return $data;
}

function getAllBilletsADMIN()
{
    $dbh = bddConnect();
    $stmt = $dbh->prepare('SELECT * FROM billets_dispo');
    $stmt->execute();
    $data = $stmt->fetchAll();

    foreach ($data as $rows) {
        $dateheuredepart = new DateTime($rows['dateheure_depart']);
        $duree = new DateTime($rows['duree_trajet']);
        $numTrain = (int)$rows['numero_train'];
        $billetResult[] = new Billet($rows['gare_depart'], $rows['gare_arrivee'], $duree, $dateheuredepart, $numTrain, $rows['quantite_dispo'], $rows['estActif'], $rows['id']);
    }

    return $billetResult;

}
