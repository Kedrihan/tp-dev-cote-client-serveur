<?php
/**
 * Created by PhpStorm.
 * User: Loic
 * Date: 30/01/2018
 * Time: 11:47
 */

function bddConnect()
{

    $database_hostname = "localhost";
    $database_user = "root";
    $database_password = "";
    $database_name = "tpeval";


    try {
        $dbh = new PDO("mysql:host=$database_hostname;dbname=$database_name;", $database_user, $database_password);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch
    (PDOException $e) {
        print "Erreur !: " . $e->getMessage() . "<br/>";
        die();
    }
    return $dbh;
}