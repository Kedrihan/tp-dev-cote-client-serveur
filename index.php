<?php

include 'base/header.php';
include 'repository/UserRepository.php';
include 'repository/BilletRepository.php';?>
<div class="container">
    <?php if(!isset($_SESSION['id'])) {
        ?>

    <div class="cadre">
        <div class="icon-lock">
            <img src="styles/lock.png" alt="cadenas"/>
        </div>
        <form method="POST" action="application/login.php">
            <div class="inscription">
                <div class="row"><?php

                    if (isset($_SESSION['error'])){
                    echo '<div class="alert alert-danger col-sm-12">'.$_SESSION['error'].'</div>';
                    $_SESSION['error'] = null;
                    }
                    if (isset($_SESSION['success'])){
                        echo '<div class="alert alert-success col-sm-12">'.$_SESSION['success'].'</div>';
                        $_SESSION['success'] = null;
                    }
                    ?>
                        <div class="input-group col-sm-12">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Email</span>
                            </div>
                            <input type="email" class="form-control" placeholder="E-mail" aria-label="Login" aria-describedby="basic-addon1" name="email" required>
                        </div>

			<div class="input-group col-sm-12">
				<div class="input-group-prepend">
					<span class="input-group-text" id="basic-addon2">Mot de passe</span>
				</div>
				<input type="password" class="form-control" placeholder="Mot de passe" aria-label="Mdp" aria-describedby="basic-addon1" name="mdp" required>
			</div>
                    <div class="idex_btn col-sm-12">
                        <div class="input-group"><button type="submit" class="btn btn-primary">Envoyer</button></div>
                        <div class="input-group">
                            <p class="col-sm-6 text-left"><a href="mdpoublie.php">Mot de passe oublié ?</a></p>
                            <p class="col-sm-6 text-right"><a href="inscription.php">Inscription</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    <?php }
    else {
        ?>
        <div class="cadre">
            <div class="row">
                <div class="col col-md-6">
                    <?php if (isset($_SESSION['error'])){
                                echo '<div class="alert alert-danger">'.$_SESSION['error'].'</div>';
                                $_SESSION['error'] = null;
                            }
                            if (isset($_SESSION['success'])){
                                echo '<div class="alert alert-success">'.$_SESSION['success'].'</div>';
                                $_SESSION['success'] = null;
                            }
                            ?>
                            <p>
                                Vous pouvez :<br>
                                <a href="profil.php">Gérer votre profil</a><br>
                                <a href="reservations.php">Voir et gérer vos réservations</a><br>
                                <a href="recherche.php">Rechercher un trajet</a><br>
            <?php
            if($_SESSION['role'] === '1') { ?>
                                <a href="listeUser.php">Administrer les utilisateurs</a><br>
                <?php       }

            ?>

                            </p>



                </div>
            </div>
        </div>
    <?php }

    ?>

    </div>
</div>
<?php

include 'base/footer.php'; ?>