<?php
/**
 * Created by PhpStorm.
 * User: Corentin
 * Date: 09/06/2018
 * Time: 22:32
 */
include 'repository/UserRepository.php';
include 'repository/BilletRepository.php';
include 'repository/ReservationRepository.php';
include 'class/Reservation.php';
include 'class/User.php';
include 'class/Billet.php';
include "class/Role.php";
include 'base/header.php';
if ($_SESSION['role'] == '1') { ?>

    <form method="post" action="admin/billetAdd.php">
        <table class="table">
            <tr>
                <th scope="col">Départ</th>
                <th scope="col">Arrivée</th>
                <th scope="col">Date et Heure</th>
                <th scope="col">Durée du trajet</th>
                <th scope="col">Numéro du train</th>
                <th scope="col">Quantité</th>
            </tr>
            <?php

                echo '<tr>';
                echo '<td scope="row"><input name="depart" type="text" required/></td>';
                echo '<td scope="row"><input name="arrivee" type="text" required/></td>';
                echo '<td scope="row"><input min="' . date('Y-m-d') . '" name="datedepart" type="date" required/> à <input name="heuredepart" type="time" min="05:00" max="23:00" required/></td>';
                echo '<td scope="row"><input name="duree" type="time" required/></td>';
                echo '<td scope="row"><input name="numTrain" type="text" required/></td>';
                echo '<td scope="row"><input name="quantite" type="text" required/></td>';
                echo '</td>';
                echo '</tr>';

            ?>
        </table>
        <button class="btn btn-primary" type="submit">Ajouter</button>


    </form>

    <?php
} else {
    $_SESSION['error'] = "Vous n'avez pas le droit d'accéder à cette page.";
    header('Location: index.php');
    die();
}


include 'base/footer.php';