<?php
/**
 * Created by PhpStorm.
 * User: Loic
 * Date: 01/03/2018
 * Time: 13:39
 */

include "base/header.php";
include 'repository/UserRepository.php';

$_SESSION['token'] = bin2hex(openssl_random_pseudo_bytes(24));

if (isset($_SESSION['error'])){
    echo '<div class="alert alert-danger">'.$_SESSION['error'].'</div>';
    $_SESSION['error'] = null;
}
if (isset($_SESSION['success'])){
    echo '<div class="alert alert-success">'.$_SESSION['success'].'</div>';
    $_SESSION['success'] = null;
}
?>

<div class"container">
    <div class="cadre edit_pass">
        <img src="styles/lock2.png" alt="reset pass"/>
        <form method="post" action="application/editPassword.php">

            <input class="text-center col-sm-12 form-control" name="previousPassword" type="password" placeholder="Ancien mot de passe">
            <input class="text-center col-sm-12 form-control" name="password" type="password" placeholder="Nouveau mot de passe">
            <input class="text-center col-sm-12 form-control" name="repassword" type="password" placeholder="Répéter le nouveau mot de passe">
            <?php
            echo '<input name="token" type="hidden" value="'. $_SESSION['token'] .'">';
            ?>
            <button type="submit" class="btn btn-primary">Enregistrer</button>
        </form>
    </div>
