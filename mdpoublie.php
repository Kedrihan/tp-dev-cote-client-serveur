<?php
include 'base/header.php';
?>

<div class="container">
    <div class="cadre">
        <div class="icon-lock">
            <img src="styles/lock2.png" alt="cadenas"/>
        </div>
        <div class="mdpoublie">
            <div class="col-sm-12"><?php
                if (isset($_SESSION['error'])){
                echo '<div class="alert alert-danger">'.$_SESSION['error'].'</div>';
                $_SESSION['error'] = null;
                }
                if (isset($_SESSION['success'])){
                    echo '<div class="alert alert-success">'.$_SESSION['success'].'</div>';
                    $_SESSION['success'] = null;
                }?>
                <form action="application/mdpoublieControl.php" method="post">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Adresse mail : </label>
                        <input type="email" class="form-control" id="exampleInputEmail1" name="email" aria-describedby="emailHelp" placeholder="Votre mail">
                    </div>
                    <button type="submit" class="btn btn-primary">Envoyer un nouveau mot de passe</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
include 'base/footer.php';
