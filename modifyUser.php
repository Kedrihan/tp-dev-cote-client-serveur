<?php
/**
 * Created by PhpStorm.
 * User: Corentin
 * Date: 28/02/2018
 * Time: 13:56
 */
include 'repository/UserRepository.php';
include 'repository/BilletRepository.php';
include 'repository/ReservationRepository.php';
include 'class/Reservation.php';
include 'class/User.php';
include 'class/Billet.php';
include "class/Role.php";
include 'base/header.php';
if ($_SESSION['role'] == '1') { ?>

    <form method="post" action="admin/userModif.php">
        <table class="table">
            <tr>
                <th scope="col">Nom</th>
                <th scope="col">Prénom</th>
                <th scope="col">Email</th>
                <th scope="col">Rôle</th>
            </tr>
            <?php

            if (in_array($_SESSION['id'], $_GET)) {
                header('Location: listeUser.php');
            }

            foreach ($_GET as $id) {
                $donnees[] = getUserById($id);
                $roles = getRole();
            }
            ?>

            <?php
            foreach ($donnees as $data) {
                echo '<input type="hidden" name="id[]" value="' . $data->getId() . '">';
                echo '<tr>';
                echo '<td scope="row">' . $data->getNom() . '</td>';
                echo '<td scope="row">' . $data->getPrenom() . '</td>';
                echo '<td scope="row">' . $data->getEmail() . '</td>';
                echo '<td scope="row">';
                echo '<select name="role[]">';
                foreach ($roles as $r) {
                    echo '<option ' . (($data->getRole() == $r->getId()) ? 'selected' : '') . '>' . $r->getLabel() . '</option>';
                }

                echo '</select></td>';
                echo '</tr>';
            }

            ?>
        </table>
        <!-- Button trigger modal confirm -->
        <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#confirmation">Modifier</button>

        <!-- Modal confirm -->
        <div class="modal fade" id="confirmation" tabindex="-1" role="dialog" aria-labelledby="confirmationSuppr"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Voulez-vous vraiment modifier le ou les rôles ?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Non</button>
                        <button type="submit" class="btn btn-primary">Oui, confirmer</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <?php
} else {
    $_SESSION['error'] = "Vous n'avez pas le droit d'accéder à cette page.";
    header('Location: index.php');
    die();
}


include 'base/footer.php';