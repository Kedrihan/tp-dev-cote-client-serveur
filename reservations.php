<?php

include 'base/header.php';
include 'repository/UserRepository.php';
include 'repository/BilletRepository.php';
include 'repository/ReservationRepository.php';
include 'class/Reservation.php';
include 'class/User.php';
include 'class/Billet.php';
?>
    <div class="container">
        <?php if (isset($_SESSION['id'])) {
            $user = getUserById($_SESSION['id']);
            $reservations = getReservationByClientId($_SESSION['id']);
            if (is_null($reservations)) {
                ?>
                <div class="cadre">
                    <div align="center">Aucune réservations</div>
                </div>
            <?php } else {
                foreach ($reservations as $res) {
                    $bil[] = getBilletById($res->getBilletId());
                }
                function cmp($a, $b)
                {
                    return date_diff($a->getDateHeureDepart(), $b->getDateHeureDepart())->invert;
                }

                usort($bil, "cmp");

                ?>
                <div class="cadre">
                    <h1>Mes réservations</h1>
                    <?php
                    if (isset($_SESSION['success'])){
                        echo '<div class="alert alert-success">'.$_SESSION['success'].'</div>';
                        $_SESSION['success'] = null;
                    }
                    if (isset($_SESSION['error'])){
                        echo '<div class="alert alert-danger">'.$_SESSION['error'].'</div>';
                        $_SESSION['error'] = null;
                    }
                    foreach ($bil as $b) {
                        $r = getBilletReservationForClient($b, $user);
                        $resaFinale = getReservationById($r[0]);
                        echo '<div class="cadre">
                                <table class="table">
                                      <thead>
                                        <tr>
                                          <th scope="col">Gare de départ</th>
                                          <th scope="col">Gare d\'arrivée</th>
                                          <th scope="col">Jour et heure de départ</th>
                                          <th scope="col">Durée du trajet</th>
                                          <th scope="col">Numéro du train</th>
                                          <th scope="col">Nombre de billets</th>
                                          <th scope="col"></th>                                     
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                          <td>'.$b->getGareDepart().'</td>
                                          <td>'.$b->getGareArrivee().'</td>
                                          <td>'.$b->getDateHeureDepart()->format('d/m/Y à H:i').'</td>
                                          <td>'.$b->getDureeTrajet()->format('H\hi').'</td>
                                          <td>'.$b->getNumeroTrain().'</td>
                                          <td>'.$resaFinale->getQuantite().'</td>
                                          <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#'.$resaFinale->getId().'">Supprimer</button></td>
                                        </tr>
                                      </tbody>
                                </table>
                            </div>
                            <!-- Modal DELETE -->
                            <div class="modal fade" id="'.$resaFinale->getId().'" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            Voulez-vous vraiment annuler votre réservation du trajet de '.$b->getGareDepart().' à '.$b->getGareArrivee().' ? Vous ne pourrez pas revenir en arrière.
                                        </div>
                                        <div    class="modal-footer">
                                            <form method="post" action="application/deleteReservation.php">
                                                <input type="hidden" name="idDelResa" value="'.$resaFinale->getId().'"/>
                                                <input type="hidden" name="idBillet" value="'.$b->getId().'"/>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Non</button>
                                                <button type="submit" class="btn btn-primary">Oui</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>';
                    } ?>
                </div>
                <?php
            }
        } else {
            $_SESSION['error'] = 'Veuillez vous connecter pour accéder à cette page';
            header('Location: index.php');
        }

        ?>
    </div>

<?php
include 'base/footer.php'; ?>