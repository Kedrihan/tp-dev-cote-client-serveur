<?php
/**
 * Created by PhpStorm.
 * User: Corentin
 * Date: 07/06/2018
 * Time: 11:22
 */

class Reservation
{
    private $id;
    private $billetId;
    private $clientId;
    private $quantite;

    public function __construct(int $billetId, int $clientId, int $quantite, ?int $id = null)
    {
        $this->billetId = $billetId;
        $this->clientId = $clientId;
        $this->quantite = $quantite;
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getBilletId(): int
    {
        return $this->billetId;
    }

    /**
     * @param int $billetId
     */
    public function setBilletId(int $billetId): void
    {
        $this->billetId = $billetId;
    }

    /**
     * @return int
     */
    public function getClientId(): int
    {
        return $this->clientId;
    }

    /**
     * @param int $clientId
     */
    public function setClientId(int $clientId): void
    {
        $this->clientId = $clientId;
    }

    /**
     * @return int
     */
    public function getQuantite(): int
    {
        return $this->quantite;
    }

    /**
     * @param int $quantite
     */
    public function setQuantite(int $quantite): void
    {
        $this->quantite = $quantite;
    }

}