<?php
/**
 * Created by PhpStorm.
 * User: Corentin
 * Date: 07/06/2018
 * Time: 09:25
 */

class Billet
{
    private $id;
    private $gareDepart;
    private $gareArrivee;
    private $dureeTrajet;
    private $dateHeureDepart;
    private $numeroTrain;
    private $quantite;
    private $estActif;

    public function __construct(string $gareDepart, string $gareArrivee, DateTime $dureeTrajet, DateTime $dateHeureDepart, int $numeroTrain, int $quantite, bool $estActif, ?int $id = null)
    {
        $this->id = $id;
        $this->gareDepart = $gareDepart;
        $this->gareArrivee = $gareArrivee;
        $this->dureeTrajet = $dureeTrajet;
        $this->dateHeureDepart = $dateHeureDepart;
        $this->numeroTrain = $numeroTrain;
        $this->quantite = $quantite;
        $this->estActif = $estActif;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getGareDepart(): string
    {
        return $this->gareDepart;
    }

    /**
     * @param string $gareDepart
     */
    public function setGareDepart(string $gareDepart): void
    {
        $this->gareDepart = $gareDepart;
    }

    /**
     * @return string
     */
    public function getGareArrivee(): string
    {
        return $this->gareArrivee;
    }

    /**
     * @param string $gareArrivee
     */
    public function setGareArrivee(string $gareArrivee): void
    {
        $this->gareArrivee = $gareArrivee;
    }

    /**
     * @return DateTime
     */
    public function getDureeTrajet(): DateTime
    {
        return $this->dureeTrajet;
    }

    /**
     * @param DateTime $dureeTrajet
     */
    public function setDureeTrajet(DateTime $dureeTrajet): void
    {
        $this->dureeTrajet = $dureeTrajet;
    }

    /**
     * @return DateTime
     */
    public function getDateHeureDepart(): DateTime
    {
        return $this->dateHeureDepart;
    }

    /**
     * @param DateTime $dateHeureDepart
     */
    public function setDateHeureDepart(DateTime $dateHeureDepart): void
    {
        $this->dateHeureDepart = $dateHeureDepart;
    }

    /**
     * @return int
     */
    public function getNumeroTrain(): int
    {
        return $this->numeroTrain;
    }

    /**
     * @param int $numeroTrain
     */
    public function setNumeroTrain(int $numeroTrain): void
    {
        $this->numeroTrain = $numeroTrain;
    }

    /**
     * @return int
     */
    public function getQuantite(): int
    {
        return $this->quantite;
    }

    /**
     * @param int $quantite
     */
    public function setQuantite(int $quantite): void
    {
        $this->quantite = $quantite;
    }

    /**
     * @return bool
     */
    public function getEstActif(): bool
    {
        return $this->estActif;
    }

    /**
     * @param bool $estActif
     */
    public function setEstActif(bool $estActif): void
    {
        $this->estActif = $estActif;
    }

}