<?php
/**
 * Created by PhpStorm.
 * User: Corentin
 * Date: 09/06/2018
 * Time: 16:11
 */

class Role
{
    private $id;
    private $label;

    public function __construct(string $label, ?int $id = null)
    {
        $this->label = $label;
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel(string $label): void
    {
        $this->label = $label;
    }
}