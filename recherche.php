<?php
include 'repository/UserRepository.php';
include 'repository/BilletRepository.php';
include 'repository/ReservationRepository.php';
include 'class/Reservation.php';
include 'class/User.php';
include 'class/Billet.php';
include 'base/header.php';

?>
    <div class="container">

        <div class="cadre">
            <h1>Recherche</h1>
            <form method="get" action="application/search.php">
                <div class="row" align="center">
                    <div class="col-md-5">

                        <label for="gareDepart">Gare de départ </label><br>
                        <select class="form-control" name="gareDepart" id="gareDepart" size="1">
                            <?php
                            $garedepart = getGaresDepart();
                            foreach ($garedepart as $g) {
                                echo '<option>' . $g['gare_depart'] . '</option>';
                            }
                            ?>

                        </select>

                        <label for="gareArrivee">Gare d'arrivée </label><br>
                        <select class="form-control" name="gareArrivee" id="gareArrivee" size="1">
                            <?php
                            $garedepart = getGaresArrivee();
                            foreach ($garedepart as $g) {
                                echo '<option>' . $g['gare_arrivee'] . '</option>';
                            }
                            ?>

                        </select>

                    </div>
                    <div class="col-md-5">
                        <label for="dateHeureDepart">Date et heure du départ</label><br>
                        <input type="date"
                               class="form-control"
                               id="dateHeureDepart"
                               name="dateDepart"
                               min="<?php echo date('Y-m-d') ?>"
                               value="<?php echo date('Y-m-d') ?>"
                               required/> à partir de
                        <select class="form-control" name="heureDepart" size="1">
                            <?php

                            for ($i = 5; $i <= 23; $i++) {
                                if ($i == date('G')) {
                                    $selected = "selected";
                                } else {
                                    $selected = "";
                                }

                                echo '<option ' . $selected . '>' . $i . 'h</option>';
                            }
                            ?>
                        </select>

                    </div>
                    <div class="col-md-2">
                        <br><br>
                        <div class="input-group">
                            <button type="submit" class="btn btn-primary">Rechercher</button>
                        </div>
                    </div>
                </div>
            </form>
            <?php

            if (isset($_SESSION['searchResult'])) {

            foreach ($_SESSION['searchResult'] as $b) {

                if (isset($_SESSION['role'])) {
                    $bouton = "<button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#".$b->getId()."\">Réserver</button>";
                } else {
                    $bouton = "<a href=\"index.php\" class=\"btn btn-primary\">Connectez-vous pour réserver</a>";
                }
                $id = (isset($_SESSION['id'])) ? $_SESSION['id'] : "";
                echo '<div class="cadre">
                                <table class="table">
                                      <thead>
                                        <tr>
                                          <th scope="col">Gare de départ</th>
                                          <th scope="col">Gare d\'arrivée</th>
                                          <th scope="col">Jour et heure de départ</th>
                                          <th scope="col">Durée du trajet</th>
                                          <th scope="col">Numéro du train</th>
                                          <th scope="col">Nombre de billets disponibles</th>
                                          <th scope="col"></th>                                     
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                          <td>' . $b->getGareDepart() . '</td>
                                          <td>' . $b->getGareArrivee() . '</td>
                                          <td>' . $b->getDateHeureDepart()->format('d/m/Y à H:i') . '</td>
                                          <td>' . $b->getDureeTrajet()->format('H\hi') . '</td>
                                          <td>' . $b->getNumeroTrain() . '</td>
                                          <td>' . $b->getQuantite() . '</td>
                                          <td>' . $bouton . '</td>
                                        </tr>
                                      </tbody>
                                </table>
                            </div>
                            <!-- Modal RESERVER -->
                            <div class="modal fade" id="' . $b->getId() . '" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form method="post" action="confirmReservation.php">
                                        <div class="modal-body">
                                            Vous allez réserver un trajet de ' . $b->getGareDepart() . ' à ' . $b->getGareArrivee() . '.<br>
                                            Choisissez le nombre de billets voulus : <br><br>
                                            <input type="number" min="1" max="' . $b->getQuantite() . '" name="quantite" required/>
                                        </div>
                                        <div    class="modal-footer">
                                            
                                                <input type="hidden" name="idUser" value="' . $id . '"/>
                                                <input type="hidden" name="idBillet" value="' . $b->getId() . '"/>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                                                <button type="submit" class="btn btn-primary">Réserver</button>
                                           
                                        </div> </form>
                                    </div>
                                </div>
                            </div>';


            } ?>
        </div><?php
        $_SESSION['searchResult'] = null;
        }
        else {
            echo '<div class="cadre" align="center">Aucun résultat</div>';
        }
        ?>
    </div>


<?php
include 'base/footer.php'; ?>