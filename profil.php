<?php
/**
 * Created by PhpStorm.
 * User: Loic
 * Date: 28/02/2018
 * Time: 16:27
 */

include "base/header.php";

include 'class/User.php';
include 'repository/UserRepository.php';

$_SESSION['token'] = bin2hex(openssl_random_pseudo_bytes(24));

$user = getUserById($_SESSION['id']);

if (isset($_SESSION['error'])){
    echo '<div class="alert alert-danger">'.$_SESSION['error'].'</div>';
    $_SESSION['error'] = null;
}
if (isset($_SESSION['success'])){
    echo '<div class="alert alert-success">'.$_SESSION['success'].'</div>';
    $_SESSION['success'] = null;
}
?>
<div class"container">
    <div class="cadre profil">
        <img src="styles/user.png" alt="user"/>
        <form method="post" action="application/editProfil.php">
            <?php
            echo '<input class="text-center col-sm-12 form-control" name="prenom" type="text" placeholder="prenom" value="'. $user->getPrenom() .'">';
            echo '<input class="text-center col-sm-12 form-control" name="nom" type="text" placeholder="nom" value="'. $user->getNom() .'">';
            echo '<input class="text-center col-sm-12 form-control" name="email" type="email" placeholder="email" value="'. $user->getEmail() .'">';
            echo '<input class="text-center col-sm-12 form-control" name="token" type="hidden" value="'. $_SESSION['token'] .'">';
            ?>
            <button type="submit" class="btn btn-primary">Enregistrer</button>
        </form>
        <a href="editUserPassword.php" class="btn btn-primary btn_profil">Changer de mot de passe</a>
    </div>
