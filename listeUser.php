
    <?php
include 'repository/UserRepository.php';
include 'repository/BilletRepository.php';
include 'repository/ReservationRepository.php';
include 'class/Reservation.php';
include 'class/User.php';
include 'class/Billet.php';
include 'base/header.php';
       $users = getAllUsers();

    ?>
    <div class="container">
    <div class="cadre">

<?php if ($_SESSION['role'] == '1') {
    if (isset($_SESSION['error'])){
        echo '<div class="alert alert-danger">'.$_SESSION['error'].'</div>';
        $_SESSION['error'] = null;
    }
    if (isset($_SESSION['success'])){
        echo '<div class="alert alert-success">'.$_SESSION['success'].'</div>';
        $_SESSION['success'] = null;
    }
    ?>
    <form method="post" action="admin/manageUsers.php"><table class="table">
                <tr>
                    <th scope="col">Sélection</th>
                    <th scope="col">Nom</th>
                    <th scope="col">Prénom</th>
                    <th scope="col">Email</th>
                    <th scope="col">Rôle</th>
                </tr>
            <?php
    foreach ($users as $u) {
        switch ($u->getRole()) {
            case 1:
                $role='Admin';
                break;
            case 2:
                $role='Utilisateur';
                break;
        }
        echo '<tr>';
        if($_SESSION['id']!=$u->getId()) {
            echo '<td scope="row"><input type="checkbox" name="id[]" value="' . $u->getId() . '"></td>';
        }
        else {
            echo '<td scope="row"></td>';
        }
        echo '<td scope="row">' . $u->getNom() . '</td>';
        echo '<td scope="row">' . $u->getPrenom() . '</td>';
        echo '<td scope="row">' . $u->getEmail() . '</td>';
        echo '<td scope="row">' . $role . '</td>';
        echo '</tr>';
    }
    ?>
        </table>
        <?php
        if (!empty($users))
        {?>
            <!-- Button trigger modal confirm -->
            <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#confirmation">Supprimer la sélection</button>

            <!-- Modal confirm -->
            <div class="modal fade" id="confirmation" tabindex="-1" role="dialog" aria-labelledby="confirmationSuppr" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            Voulez-vous vraiment supprimer la sélection ?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Non</button>
                            <button type="submit" class="btn btn-primary" name="delete">Oui, confirmer</button>
                        </div>
                    </div>
                </div>
            </div>
            <button class="btn btn-primary" type="submit" name="modify">Modifier les rôles de la sélection</button>
        <?php
        }
        else {
            $_SESSION['error'] = "Erreur";
            header('Location: listeUser.php');
            die;
        }
        ?>
    </form>
    </div>

    <?php }
    else {
        $_SESSION['error'] = "Vous n'avez pas le droit d'accéder à cette page";
        header('Location: index.php');
        die();
    }
    include 'base/footer.php';
