 <?php
    include 'base/header.php';
    ?>
<div class="container">
    <div class="cadre">
        <?php
        if (isset($_SESSION['error'])){
            echo '<div class="alert alert-danger">'.$_SESSION['error'].'</div>';
            $_SESSION['error'] = null;
        }

        ?>
        <form class="needs-validation" action="application/register.php" method="POST">
            <div class="form-row">
                <div class="col-md-4 mb-3">
                    <label>Nom</label>
                    <input type="text" class="form-control" placeholder="Nom" name="nom" required>
                </div>
                <div class="col-md-4 mb-3">
                    <label>Prénom</label>
                    <input type="text" class="form-control" placeholder="Prénom" name="prenom" required>
                </div>
                <div class="col-md-4 mb-3">
                    <label>Adresse mail</label>
                    <input type="email" class="form-control" name="email"  placeholder="Adresse mail">
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-4 mb-3">
                    <div class="form-group">
                        <label>Mot de passe</label>
                        <input type="password" class="form-control" name="password" placeholder="Mot de passe">
                    </div>
                </div>
                <div class="col-md-4 mb-3">
                    <div class="form-group">
                        <label>Confirmation mot de passe</label>
                        <input type="password" class="form-control" name="repassword" placeholder="Confirmation mot de passe">
                    </div>
                </div>
            </div>
            <button class="btn btn-primary" type="submit">Envoyer</button>
        </form>
    </div>
</div>
 <?php
 include 'base/footer.php';

