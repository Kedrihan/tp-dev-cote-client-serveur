<?php session_start();
/**
 * Created by PhpStorm.
 * User: Corentin
 * Date: 28/02/2018
 * Time: 14:27
 */
include '../repository/UserRepository.php';
include '../repository/BilletRepository.php';
include '../repository/ReservationRepository.php';
include '../class/Reservation.php';
include '../class/User.php';
include '../class/Billet.php';
include "../class/Role.php";

if (!isset($_SESSION['id']) || ($_SESSION['role'] != '1')) {
    $_SESSION['error'] = "Vous n'avez pas le droit d'accéder à cette page";
    header('Location: ../index.php');
    die();
}

if (!empty($_POST['depart']) && !empty($_POST['arrivee']) && !empty($_POST['datedepart']) && !empty($_POST['heuredepart']) && !empty($_POST['numTrain']) && !empty($_POST['quantite']) && !empty($_POST['duree'])) {
    $rows = count($_POST['id']);


    for ($i = 0; $i < $rows; $i++) {
        $depart = $_POST['depart'][$i];
        $arrivee = $_POST['arrivee'][$i];
        $dateheuredepart = $_POST['datedepart'][$i] . ' ' . $_POST['heuredepart'][$i];
        $numtrain = $_POST['numTrain'][$i];
        $quantite = $_POST['quantite'][$i];
        $duree = $_POST['duree'][$i];
        $r = getBilletById($_POST['id'][$i]);

        if ($r->getId() == $_POST['id'][$i] && ($r->getGareDepart() != $depart || $r->getGareArrivee() != $arrivee || $r->getDateHeureDepart()->format('Y-m-d H:i') != $dateheuredepart || $r->getNumeroTrain() != $numtrain || $r->getQuantite() != $quantite || $r->getDureeTrajet()->format('H:i') != $duree)) {
            $r->setGareDepart($depart);
            $r->setGareArrivee($arrivee);
            $r->setDateHeureDepart(new DateTime($dateheuredepart));
            $r->setNumeroTrain($numtrain);
            $r->setQuantite($quantite);
            $r->setDureeTrajet(new DateTime($duree));
            updateBillet($r);
        }

    }
    $_SESSION['success'] = 'Billet(s) modifié(s) avec succès !';
    header('Location: ../adminBillets.php');
} else {
    $_SESSION['error'] = 'Erreur, veuillez recommencer';
    header('Location: ../adminBillets.php');
}