<?php session_start();
/**
 * Created by PhpStorm.
 * User: Corentin
 * Date: 28/02/2018
 * Time: 14:27
 */
include '../repository/UserRepository.php';
include '../repository/BilletRepository.php';
include '../repository/ReservationRepository.php';
include '../class/Reservation.php';
include '../class/User.php';
include '../class/Billet.php';
include "../class/Role.php";

if(!isset($_SESSION['id']) || ($_SESSION['role'] != '1')) {
    $_SESSION['error'] = "Vous n'avez pas le droit d'accéder à cette page";
    header('Location: ../index.php');
    die();
}

if (!empty($_POST['role'])) {
    $rows = count($_POST['id']);


    for ($i=0;$i<$rows;$i++) {
        switch ($_POST['role'][$i]) {
            case 'ROLE_USER':
                $role = 2;
                break;
            case 'ROLE_ADMIN':
                $role = 1;
                break;
        }
        $r = getUserById($_POST['id'][$i]);

        if ($r->getId() == $_POST['id'][$i] && $r->getRole() != $role) {
            $r->setRole($role);
            updateUser($r);
        }

    }
    $_SESSION['success'] = 'Rôle(s) modifié(s) avec succès !';
    header('Location: ../listeUser.php');
}  else {
    $_SESSION['error'] = 'Erreur, veuillez recommencer';
    header('Location: ../listeUser.php');
}