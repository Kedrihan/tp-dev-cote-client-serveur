<?php
session_start();
/**
 * Created by PhpStorm.
 * User: Corentin
 * Date: 30/01/2018
 * Time: 14:19
 */
include '../repository/UserRepository.php';
include '../repository/BilletRepository.php';
include '../repository/ReservationRepository.php';
include '../class/Reservation.php';
include '../class/User.php';
include '../class/Billet.php';
include "../class/Role.php";
if (!isset($_SESSION['id']) || ($_SESSION['role'] != '1')) {
    $_SESSION['error'] = "Vous n'avez pas le droit d'accéder à cette page";
    header('Location: ../index.php');
    die();
}

if (isset($_POST['delete'])) {
    if (!empty($_POST['id'])) {
        foreach ($_POST['id'] AS $i) {
            deleteBillet(getBilletById($i));
            $_SESSION['success'] = "Billet supprimé avec succès";
            header('Location: ../adminBillets.php');
        }
    } else {
        $_SESSION['error'] = "Pas de case cochée";
        header('Location: ../adminBillets.php');
        die();
    }
}
if (isset($_POST['modify'])) {
    /*foreach($_POST['id'] as $i) {
        $id =
    }*/
    if (!empty($_POST['id'])) {
        ini_set('arg_separator.output', '&');
        header('Location: ../modifyBillets.php?' . http_build_query($_POST['id']));
    } else {
        $_SESSION['error'] = "Pas de case cochée";
        header('Location: ../adminBillets.php');
        die();
    }
}
if (isset($_POST['add'])) {
    header('Location: ../adminAddBillets.php');
}
?>