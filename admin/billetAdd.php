<?php session_start();
/**
 * Created by PhpStorm.
 * User: Corentin
 * Date: 10/06/2018
 * Time: 00:11
 */
include '../repository/UserRepository.php';
include '../repository/BilletRepository.php';
include '../repository/ReservationRepository.php';
include '../class/Reservation.php';
include '../class/User.php';
include '../class/Billet.php';
include "../class/Role.php";

if (!isset($_SESSION['id']) || ($_SESSION['role'] != '1')) {
    $_SESSION['error'] = "Vous n'avez pas le droit d'accéder à cette page";
    header('Location: ../index.php');
    die();
}
if (!empty($_POST['depart']) && !empty($_POST['arrivee']) && !empty($_POST['datedepart']) && !empty($_POST['heuredepart']) && !empty($_POST['numTrain']) && !empty($_POST['quantite']) && !empty($_POST['duree'])) {
    $depart = $_POST['depart'];
    $arrivee = $_POST['arrivee'];
    $dateheuredepart = $_POST['datedepart'] . ' ' . $_POST['heuredepart'];
    $numtrain = $_POST['numTrain'];
    $quantite = $_POST['quantite'];
    $duree = $_POST['duree'];
    $r = new Billet($depart, $arrivee, new DateTime($duree), new DateTime($dateheuredepart), $numtrain, $quantite, 1);
    addBillet($r);
    $_SESSION['success'] = 'Billet ajouté avec succès !';
    header('Location: ../adminBillets.php');
} else {
    $_SESSION['error'] = 'Erreur, veuillez recommencer';
    header('Location: ../adminBillets.php');
}