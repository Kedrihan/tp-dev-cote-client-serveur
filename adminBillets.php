
    <?php
include 'repository/UserRepository.php';
include 'repository/BilletRepository.php';
include 'repository/ReservationRepository.php';
include 'class/Reservation.php';
include 'class/User.php';
include 'class/Billet.php';
include 'base/header.php';
       $billets = getAllBilletsADMIN();

    ?>
    <div class="container">
    <div class="cadre">

<?php if ($_SESSION['role'] == '1') {
    if (isset($_SESSION['error'])){
        echo '<div class="alert alert-danger">'.$_SESSION['error'].'</div>';
        $_SESSION['error'] = null;
    }
    if (isset($_SESSION['success'])){
        echo '<div class="alert alert-success">'.$_SESSION['success'].'</div>';
        $_SESSION['success'] = null;
    }
    ?>
    <form method="post" action="admin/manageBillets.php"><table class="table">
    <button class="btn btn-primary" type="submit" name="add">Ajouter un billet</button>
                <tr>
                    <th scope="col">Sélection</th>
                    <th scope="col">Départ</th>
                    <th scope="col">Arrivée</th>
                    <th scope="col">Date et Heure</th>
                    <th scope="col">Numéro de train</th>
                    <th scope="col">Quantité</th>
                </tr>
            <?php
    foreach ($billets as $b) {

        echo '<tr>';
        echo '<td scope="row"><input type="checkbox" name="id[]" value="' . $b->getId() . '"></td>';
        echo '<td scope="row">' . $b->getGareDepart() . '</td>';
        echo '<td scope="row">' . $b->getGareArrivee() . '</td>';
        echo '<td scope="row">' . $b->getDateHeureDepart()->format('d/m/Y à H:i') . '</td>';
        echo '<td scope="row">' . $b->getNumeroTrain() . '</td>';
        echo '<td scope="row">' . $b->getQuantite() . '</td>';
        echo '</tr>';
    }
    ?>
        </table>
        <?php
        if (!empty($billets))
        {?>
            <!-- Button trigger modal confirm -->
            <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#confirmation">Supprimer la sélection</button>

            <!-- Modal confirm -->
            <div class="modal fade" id="confirmation" tabindex="-1" role="dialog" aria-labelledby="confirmationSuppr" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            Voulez-vous vraiment supprimer la sélection ?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Non</button>
                            <button type="submit" class="btn btn-primary" name="delete">Oui, confirmer</button>
                        </div>
                    </div>
                </div>
            </div>
            <button class="btn btn-primary" type="submit" name="modify">Modifier les billets de la sélection</button>
        <?php
        }
        else {
            $_SESSION['error'] = "Erreur";
            //header('Location: listeUser.php');
            die;
        }
        ?>
    </form>
    </div>

    <?php }
    else {
        $_SESSION['error'] = "Vous n'avez pas le droit d'accéder à cette page";
        header('Location: index.php');
        die();
    }
    include 'base/footer.php';
