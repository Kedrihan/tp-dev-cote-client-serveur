<?php
session_start();
include '../repository/UserRepository.php';
include '../class/User.php';
$user = new User(null, null, $_POST['email'],null, $_POST['mdp']);
if (!empty($user->getEmail()) && !empty($user->getMdp())) {

    if (!checkMailExists($user)) {
        $_SESSION['error'] = 'Le login n\'existe pas.';
        header('Location: ../index.php');
    } else {

        if (!loginUser($user)) {
            $_SESSION['error'] = 'Le mot de passe est incorrect.';
            header('Location: ../index.php');


        } else {

            $userLogged = loginUser($user);
            $_SESSION['role'] = $userLogged->getRole();
            $_SESSION['email'] = $userLogged->getEmail();
            $_SESSION['id'] = $userLogged->getId();
            $_SESSION['prenom'] = $userLogged->getPrenom();
            $_SESSION['success'] = 'Vous vous êtes connecté avec succès !';
            header('Location: ../index.php');
        }
    }
}
