<?php
/**
 * Created by PhpStorm.
 * User: Loic
 * Date: 28/02/2018
 * Time: 16:46
 */
session_start();

include '../class/User.php';
include '../repository/UserRepository.php';

$user = new User($_POST['prenom'], $_POST['nom'], $_POST['email'], null);

$userLogged = getUserById($_SESSION['id']);

if (empty($userLogged->getId() && $user->getPrenom() && $user->getNom() && $user->getEmail())) {
    $_SESSION['error'] = 'veuillez remplir tous les champs';
    header('Location: ../profil.php');
    die();
}

if (!isset($_POST['token'])) {
    new Exception('No token found!');
    die();
}

if (strcasecmp($_POST['token'], $_SESSION['token']) != 0) {
    new Exception('Token mismatch!');
    die();
}


if (!checkIfMailExistsAnotherUser($user)) {
    try {
        $userLogged->setNom($user->getNom());
        $userLogged->setPrenom($user->getPrenom());
        $userLogged->setEmail($user->getEmail());

        updateUser($userLogged);
        $_SESSION['success'] = "Profil modifié avec succès";
        header('Location: ../profil.php');
    } catch (exception $e) {
        print "Erreur ! : " . $e->getMessage() . "<br />";
        die();
    }
} else {
    $_SESSION['error'] = 'l\'email est déja utilisé';
    header('Location: ../profil.php');
}
