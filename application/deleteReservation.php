<?php
/**
 * Created by PhpStorm.
 * User: Corentin
 * Date: 07/06/2018
 * Time: 17:59
 */
include '../repository/ReservationRepository.php';
include '../class/Reservation.php';
include '../class/Billet.php';

if(isset($_POST['idDelResa'])) {
    annulationReservation($_POST['idDelResa'], $_POST['idBillet']);
    $_SESSION['succes'] = "Réservation supprimée avec succès";
    header('Location: ../reservations.php');
}
