<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
</head>
<body>
<?php
include '../repository/UserRepository.php';
include '../repository/BilletRepository.php';
include '../repository/ReservationRepository.php';
include '../class/Reservation.php';
include '../class/User.php';
include '../class/Billet.php';
// verifier si l'eamil existe avant de creer un mot de passe
$mailUser = $_POST['email'];
$user = getUserByEmail($mailUser);
if (!checkMailExists($user)) {
    $_SESSION['error'] = "L'utilisateur n'existe pas";
    header('Location: ../mdpoublie.php');
} else {
    $newMDP = generatePwd(8);

    $user->setMdp($newMDP);
    updateUser($user);
    $_SESSION['success'] = 'Votre nouveau mot de passe est : ' . $newMDP;
    header('Location: ../mdpoublie.php');
}


?>
</body>
</html>
