<?php
/**
 * Created by PhpStorm.
 * User: Loic
 * Date: 01/03/2018
 * Time: 13:44
 */

session_start();
include '../repository/UserRepository.php';
include '../class/User.php';
$user = getUserById($_SESSION['id']);
if (empty($_POST['previousPassword'] && $_POST['password'] && $_POST['repassword'])) {
    $_SESSION['error'] = 'veuillez remplir tous les champs';
    header('Location: ../editUserPassword.php');
    die();
}

if (!isset($_POST['token'])) {
    new Exception('No token found!');
    die();
}

if (strcasecmp($_POST['token'], $_SESSION['token']) != 0) {
    new Exception('Token mismatch!');
    die;
}

if ($_POST['password'] === $_POST['previousPassword']) {
    $_SESSION['error'] = 'L\'ancien et le nouveau mot de passe sont identiques';
    header('Location: ../editUserPassword.php');
    die();
}

if ($_POST['password'] !== $_POST['repassword']) {
    $_SESSION['error'] = 'Les mots de passes ne correspondent pas';
    header('Location: ../editUserPassword.php');
    die();
}

$previousPassword = hash('sha1', $_POST['previousPassword']);
if ($user->getMdp() !== $previousPassword) {
    $_SESSION['error'] = 'Veuillez entrer votre ancien mot de passe';
    header('Location: ../editUserPassword.php');
    die();
}
else {
    $user->setMdp($_POST['password']);
    updateUser($user);
    $_SESSION['success'] = "Vous avez modifié votre mot de passe avec succès !";
    header('Location: ../editUserPassword.php');
}