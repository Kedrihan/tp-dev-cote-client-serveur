<?php
/**
 * Created by PhpStorm.
 * User: Corentin
 * Date: 09/06/2018
 * Time: 15:18
 */
include '../repository/UserRepository.php';
include '../repository/BilletRepository.php';
include '../repository/ReservationRepository.php';
include '../class/Reservation.php';
include '../class/User.php';
include '../class/Billet.php';
session_start();

$resa = new Reservation($_POST['billet'], $_POST['user'], $_POST['quantite']);
$verif = verifyDuplicata($resa);
if(!$verif) {
    addReservation($resa);
    $_SESSION['success'] = "Réservation réalisée avec succès";
    header('Location: ../reservations.php');
}
else {
    $_SESSION['error'] = "Vous ne pouvez pas réserver plusieurs fois le même billet";
    header('Location: ../reservations.php');
}
