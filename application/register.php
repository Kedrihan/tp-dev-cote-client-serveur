<?php
/**
 * Created by PhpStorm.
 * User: Loic
 * Date: 30/01/2018
 * Time: 11:48
 */
session_start();
include '../class/User.php';
include '../repository/UserRepository.php';

$newUser = new User($_POST['prenom'], $_POST['nom'], $_POST['email'],null, $_POST['password']);
if((empty($newUser->getMdp()) || empty($_POST['repassword'])) || ($newUser->getMdp() !== $_POST['repassword'])) {
    $_SESSION['error'] = "Les mots de passe ne sont pas identiques";
    header('Location: ../inscription.php');
    die();
}

if(empty($newUser->getNom()) && empty($newUser->getPrenom()) && empty($newUser->getEmail()) && empty($newUser->getMdp()))
{
    $_SESSION['error'] = "Veuillez remplir tous les champs";
    header('Location: ../inscription.php');
    die();
}

if (checkMailExists($newUser)) {
    $_SESSION['error'] = "L'email est déjà utilisé";
    header('Location: ../inscription.php');
    die();
}

if ($_POST['repassword'] === $newUser->getMdp()) {
    addUser($newUser);
    $_SESSION['success'] = "Vous êtes inscrit, vous pouvez vous connecter !";
    header('Location: ../index.php');

}
?>
