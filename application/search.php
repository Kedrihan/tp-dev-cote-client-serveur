<?php
/**
 * Created by PhpStorm.
 * User: Corentin
 * Date: 08/06/2018
 * Time: 09:46
 */
include '../repository/BilletRepository.php';
include '../class/Billet.php';
session_start();

$gareDepartSearch = $_GET['gareDepart'];
$gareArriveeSearch = $_GET['gareArrivee'];
$dateDepart = $_GET['dateDepart'];
$heureMin = $_GET['heureDepart'];

$min_length = 3;

if(!isset($heureMin) && !isset($dateDepart)) {
    $dateHeureMin = new DateTime();
}
else {
    $posH = strpos($heureMin,'h');
    $heureMin = substr($heureMin,0,$posH);
    $concat = $dateDepart.' '.$heureMin;
    $dateHeureMin = date_create_from_format('Y-m-d H',$concat);
}

if (((strlen($gareArriveeSearch) >= $min_length) AND (strlen($gareDepartSearch) >= $min_length)) AND (isset($gareDepartSearch) AND isset($gareArriveeSearch))) {

    $gareDepartSearch = htmlspecialchars($gareDepartSearch);
    $gareArriveeSearch = htmlspecialchars($gareArriveeSearch);

    $resultSearch = searchBillet($gareDepartSearch, $gareArriveeSearch, $dateHeureMin);

    $_SESSION['searchResult'] = $resultSearch;
    header('Location: ../recherche.php');

}
else{
    $_SESSION['error'] = "Veuillez renseigner la gare de départ et la gare d'arrivée.";
    header('Location: ../recherche.php');
}