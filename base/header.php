<?php session_start(); ?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Réservation de billets de train</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="description" content="Bootstrap test"/>
    <script src="styles/jquery-3.3.1.min.js"></script>
    <script src="styles/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="styles/css/bootstrap.css"/>
    <link rel="stylesheet" href="styles/style.css"/>
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="index.php">Accueil <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="recherche.php">Rechercher un trajet <span class="sr-only">(current)</span></a>
            </li>
            <?php
            if (isset($_SESSION['role'])) {
                echo '<li class="nav-item active">
                                    <a class="nav-link" href="profil.php">Profil<span class="sr-only">(current)</span></a>
                                    </li>
                             

                           
                                <li class="nav-item active">
                                    <a class="nav-link" href="reservations.php">Mes réservations <span class="sr-only">(current)</span></a>
                                    </li>';
            }

            ?>

        </ul>
        <ul class="navbar-nav ml-auto">
            <?php
            if (isset($_SESSION['prenom'])) {
                if ($_SESSION['role'] == '1') {
                    echo '<li class="nav-item active">
                                    <a class="nav-link" href="listeUser.php">Administration des utilisateurs<span
                                                class="sr-only">(current)</span></a>
                                </li>';
                }
                if ($_SESSION['role'] == '1') {
                    echo '<li class="nav-item active">
                                    <a class="nav-link" href="adminBillets.php">Administration des billets<span
                                                class="sr-only">(current)</span></a>
                                </li>';
                }
                ?>
                <li>
                    <div class="nav-link"><?php
                        echo $_SESSION['prenom'];
                        ?></div>
                </li>
                <li>
                    <form action="application/deconnexion.php" method="post">
                        <button type="submit" class="btn btn-default navbar-btn ml-auto">Déconnexion</button>
                    </form>
                </li>
                <?php
            }
            ?>
        </ul>
    </div>
</nav>