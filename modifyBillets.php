<?php
/**
 * Created by PhpStorm.
 * User: Corentin
 * Date: 09/06/2018
 * Time: 22:32
 */
include 'repository/UserRepository.php';
include 'repository/BilletRepository.php';
include 'repository/ReservationRepository.php';
include 'class/Reservation.php';
include 'class/User.php';
include 'class/Billet.php';
include "class/Role.php";
include 'base/header.php';
if ($_SESSION['role'] == '1') { ?>

    <form method="post" action="admin/billetModif.php">
        <table class="table">
            <tr>
                <th scope="col">Départ</th>
                <th scope="col">Arrivée</th>
                <th scope="col">Date et Heure</th>
                <th scope="col">Durée du trajet</th>
                <th scope="col">Numéro du train</th>
                <th scope="col">Quantité</th>
            </tr>
            <?php

            foreach ($_GET as $id) {
                $donnees[] = getBilletById($id);
            }
            ?>

            <?php
            foreach ($donnees as $data) {
                echo '<input type="hidden" name="id[]" value="' . $data->getId() . '">';
                echo '<tr>';
                echo '<td scope="row"><input name="depart[]" type="text" value="' . $data->getGareDepart() . '" required/></td>';
                echo '<td scope="row"><input name="arrivee[]" type="text" value="' . $data->getGareArrivee() . '" required/></td>';
                echo '<td scope="row"><input min="' . date('Y-m-d') . '" name="datedepart[]" type="date" value="' . $data->getDateHeureDepart()->format('Y-m-d') . '" required/> à <input name="heuredepart[]" type="time" value="' . $data->getDateHeureDepart()->format('H:i') . '" required/></td>';
                echo '<td scope="row"><input name="duree[]" type="time" value="' . $data->getDureeTrajet()->format('H:i') . '" required/></td>';
                echo '<td scope="row"><input name="numTrain[]" type="text" value="' . $data->getNumeroTrain() . '" required/></td>';
                echo '<td scope="row"><input name="quantite[]" type="text" value="' . $data->getQuantite() . '" required/></td>';
                echo '</td>';
                echo '</tr>';
            }

            ?>
        </table>
        <!-- Button trigger modal confirm -->
        <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#confirmation">Modifier</button>

        <!-- Modal confirm -->
        <div class="modal fade" id="confirmation" tabindex="-1" role="dialog" aria-labelledby="confirmationSuppr"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Voulez-vous vraiment modifier le ou les billets ?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Non</button>
                        <button type="submit" class="btn btn-primary">Oui, confirmer</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <?php
} else {
    $_SESSION['error'] = "Vous n'avez pas le droit d'accéder à cette page.";
    header('Location: index.php');
    die();
}


include 'base/footer.php';