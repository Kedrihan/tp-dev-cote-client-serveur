<?php
include 'repository/UserRepository.php';
include 'repository/BilletRepository.php';
include 'repository/ReservationRepository.php';
include 'class/Reservation.php';
include 'class/User.php';
include 'class/Billet.php';
include 'base/header.php';
if (isset($_SESSION['role'])) {

    $quantite = $_POST['quantite'];
    $user = getUserById($_POST['idUser']);
    $billet = getBilletById($_POST['idBillet']);
    ?>

    <div class="container">

        <div class="cadre">
            <h1>Récapitulatif de la réservation</h1>
            <?php
            echo '<table class="table">
                <thead>
                <tr>
                    <th scope="col">Gare de départ</th>
                    <th scope="col">Gare d\'arrivée</th>
                    <th scope="col">Jour et heure de départ</th>
                    <th scope="col">Durée du trajet</th>
                    <th scope="col">Numéro du train</th>
                    <th scope="col">Nombre de billets réservés</th>
                    <th scope="col">Client</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>' . $billet->getGareDepart() . '</td>
                    <td>' . $billet->getGareArrivee() . '</td>
                    <td>' . $billet->getDateHeureDepart()->format('d/m/Y à H:i') . '</td>
                    <td>' . $billet->getDureeTrajet()->format('H\hi') . '</td>
                    <td>' . $billet->getNumeroTrain() . '</td>
                    <td>' . $quantite . '</td>
                    <td>' . $user->getPrenom() . ' ' . $user->getNom() . '</td>
                </tr>
                </tbody>
            </table>
            <form method="post" action="application/addReservation.php">
                <input type="hidden" name="user" value="' . $user->getId() . '"/>
                <input type="hidden" name="billet" value="' . $billet->getId() . '"/>
                <input type="hidden" name="quantite" value="' . $quantite . '"/>
                <button type="submit" class="btn btn-primary">Valider</button>
                <a href="recherche.php" class="btn btn-secondary">Retour</a>
            </form>'; ?>

        </div>
    </div>


    <?php
}
include 'base/footer.php'; ?>