CREATE TABLE role (
  `id`       INT          NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `nom_role` VARCHAR(255) NOT NULL
);
CREATE TABLE users (
  `id`       int          NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `prenom`   varchar(30)  NOT NULL,
  `nom`      varchar(30)  NOT NULL,
  `email`    varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role`     int          not null,
  FOREIGN KEY (role) REFERENCES role (id)
);
CREATE TABLE billets_dispo (
  `id`               INT          not null primary key AUTO_INCREMENT,
  `gare_depart`      varchar(255) not null,
  `gare_arrivee`     varchar(255) not null,
  `dateheure_depart` DATETIME     not null,
  `duree_trajet`     TIME         not null,
  `quantite_dispo`   INT          not null,
  `numero_train`     BIGINT       not null,
  `estActif`         BOOLEAN      not null
);
CREATE TABLE reservation (
  `id`       INT not null primary key AUTO_INCREMENT,
  `billet`   int not null,
  `quantite` int not null,
  `client`   INT not NULL,
  FOREIGN KEY (client) REFERENCES users (id),
  FOREIGN KEY (billet) REFERENCES billets_dispo (id)
);


ALTER TABLE users
  CONVERT TO CHARACTER SET utf8
  COLLATE utf8_unicode_ci;
ALTER TABLE role
  CONVERT TO CHARACTER SET utf8
  COLLATE utf8_unicode_ci;
ALTER TABLE billets_dispo
  CONVERT TO CHARACTER SET utf8
  COLLATE utf8_unicode_ci;
ALTER TABLE reservation
  CONVERT TO CHARACTER SET utf8
  COLLATE utf8_unicode_ci;

INSERT INTO `role` (`id`, `nom_role`) VALUES (NULL, 'ROLE_ADMIN');
INSERT INTO `role` (`id`, `nom_role`) VALUES (NULL, 'ROLE_USER');

INSERT INTO `users` (`prenom`, `nom`, `email`, `password`, `role`)
VALUES ('admin', 'admin', 'admin@admin.fr', 'd033e22ae348aeb5660fc2140aec35850c4da997', 1);

INSERT INTO `billets_dispo` (`gare_depart`, `gare_arrivee`, `dateheure_depart`, `duree_trajet`, `quantite_dispo`, `numero_train`, `estActif`)
VALUES ('Marne la Vallee','Toulouse','2018-07-25 10:55','2:45','400','23658',1);

INSERT INTO `billets_dispo` (`gare_depart`, `gare_arrivee`, `dateheure_depart`, `duree_trajet`, `quantite_dispo`, `numero_train`, `estActif`)
VALUES ('Nantes','Bordeaux','2018-07-20 07:34','3:15','300','54769',1);

INSERT INTO `billets_dispo` (`gare_depart`, `gare_arrivee`, `dateheure_depart`, `duree_trajet`, `quantite_dispo`, `numero_train`, `estActif`)
VALUES ('Lyon Part-Dieu','Rome','2018-10-10 21:55','4:05','700','36547',1);

INSERT INTO `billets_dispo` (`gare_depart`, `gare_arrivee`, `dateheure_depart`, `duree_trajet`, `quantite_dispo`, `numero_train`, `estActif`)
VALUES ('Rome','Lyon Part-Dieu','2018-07-28 20:10','4:54','150','98736',1);

INSERT INTO `billets_dispo` (`gare_depart`, `gare_arrivee`, `dateheure_depart`, `duree_trajet`, `quantite_dispo`, `numero_train`, `estActif`)
VALUES ('Londres St Pancras','Paris Gare du Nord','2018-07-29 14:32','5:06','50','12045',1);

INSERT INTO `billets_dispo` (`gare_depart`, `gare_arrivee`, `dateheure_depart`, `duree_trajet`, `quantite_dispo`, `numero_train`, `estActif`)
VALUES ('Lyon Perrache','Toulouse','2018-07-30 10:24','4:04','150','54129',1);

INSERT INTO `billets_dispo` (`gare_depart`, `gare_arrivee`, `dateheure_depart`, `duree_trajet`, `quantite_dispo`, `numero_train`, `estActif`)
VALUES ('Lyon Perrache','St Etienne Chateaucreux','2018-08-05 12:55','7:01','540','36471',1);

INSERT INTO `billets_dispo` (`gare_depart`, `gare_arrivee`, `dateheure_depart`, `duree_trajet`, `quantite_dispo`, `numero_train`, `estActif`)
VALUES ('Strasbourg','Paris Gare de l''Est','2018-08-14 13:50','3:32','650','64785',1);

INSERT INTO `billets_dispo` (`gare_depart`, `gare_arrivee`, `dateheure_depart`, `duree_trajet`, `quantite_dispo`, `numero_train`, `estActif`)
VALUES ('Bruxelles','Lille','2018-08-12 14:10','5:02','120','96014',1);

INSERT INTO `billets_dispo` (`gare_depart`, `gare_arrivee`, `dateheure_depart`, `duree_trajet`, `quantite_dispo`, `numero_train`, `estActif`)
VALUES ('Lille','Rennes','2018-08-22 11:04','8:32','640','14970',1);

INSERT INTO `billets_dispo` (`gare_depart`, `gare_arrivee`, `dateheure_depart`, `duree_trajet`, `quantite_dispo`, `numero_train`, `estActif`)
VALUES ('Rennes','Brest','2018-08-23 12:55','2:35','400','36470',1);

INSERT INTO `billets_dispo` (`gare_depart`, `gare_arrivee`, `dateheure_depart`, `duree_trajet`, `quantite_dispo`, `numero_train`, `estActif`)
VALUES ('Marseille','Aix-en-Provence','2018-08-23 07:55','3:45','124','01254',1);

INSERT INTO `billets_dispo` (`gare_depart`, `gare_arrivee`, `dateheure_depart`, `duree_trajet`, `quantite_dispo`, `numero_train`, `estActif`)
VALUES ('Nice','Valence','2018-09-24 08:08','2:15','300','10478',1);

INSERT INTO `billets_dispo` (`gare_depart`, `gare_arrivee`, `dateheure_depart`, `duree_trajet`, `quantite_dispo`, `numero_train`, `estActif`)
VALUES ('Arles','Marseille','2018-09-25 12:55','2:52','74','65278',1);

INSERT INTO `billets_dispo` (`gare_depart`, `gare_arrivee`, `dateheure_depart`, `duree_trajet`, `quantite_dispo`, `numero_train`, `estActif`)
VALUES ('Arras','Lille','2018-09-27 11:20','1:05','120','45871',1);

INSERT INTO `billets_dispo` (`gare_depart`, `gare_arrivee`, `dateheure_depart`, `duree_trajet`, `quantite_dispo`, `numero_train`, `estActif`)
VALUES ('Toulouse','Bordeaux','2018-09-28 05:55','0:52','450','74125',1);

INSERT INTO `billets_dispo` (`gare_depart`, `gare_arrivee`, `dateheure_depart`, `duree_trajet`, `quantite_dispo`, `numero_train`, `estActif`)
VALUES ('Perpignan','Toulouse','2018-09-30 09:45','4:54','650','65481',1);

INSERT INTO `billets_dispo` (`gare_depart`, `gare_arrivee`, `dateheure_depart`, `duree_trajet`, `quantite_dispo`, `numero_train`, `estActif`)
VALUES ('Lyon Part-Dieu','Marne la Vallee','2018-10-04 16:22','3:23','120','15975',1);

INSERT INTO `billets_dispo` (`gare_depart`, `gare_arrivee`, `dateheure_depart`, `duree_trajet`, `quantite_dispo`, `numero_train`, `estActif`)
VALUES ('Aix-en-Provence','Arles','2018-10-12 13:42','2:24','500','35798',1);

INSERT INTO `billets_dispo` (`gare_depart`, `gare_arrivee`, `dateheure_depart`, `duree_trajet`, `quantite_dispo`, `numero_train`, `estActif`)
VALUES ('St Etienne Chateaucreux','Paris Gare de Lyon','2018-10-14 17:30','2:15','100','49761',1);